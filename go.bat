
@echo off


REM open putty connection
if "%1" == "ssh" (
  start C:\Users\Chris\Desktop\software\PuTTY\putty.exe -load %2 
  exit
) 

REM Open sublime text project
if "%1" == "sub" (
  if "%2" == "srv" (
		"C:\Users\Chris\Desktop\htdocs\sublime projects\WEBSERVER CONFIG.sublime-project"
	) 
  if "%2" == "poosh" (
		"C:\Users\Chris\Desktop\htdocs\sublime projects\POOSH.sublime-project"
	)	
  if "%2" == "brtc" (
		"C:\Users\Chris\Desktop\htdocs\sublime projects\BRTC.sublime-project"
	)	
  if "%2" == "cfs" (
		"C:\Users\Chris\Desktop\htdocs\sublime projects\TOURN REG plus FORMS.sublime-project"
	)	
  if "%2" == "d8" (
		"C:\Users\Chris\Desktop\htdocs\sublime projects\D8ONVM.sublime-project"
	)		
) 

REM Open ftp connection
if "%1" == "ftp" (
  if "%2" == "brtc" (
		"C:\Program Files (x86)\FileZilla FTP Client\filezilla.exe" ^
		"ftp://bwayrose:453Thur$@broadwayrose.org/domains/broadwayrose.org/public_html" 
	) 
)


REM Open sql client
if "%1" == "db" (
  if "%2" == "poosh" (
		"C:\Program Files (x86)\MySQL\MySQL Workbench 5.2 CE\MySQLWorkbench.exe" ^
		-query dev.poosh
	) 
)

REM open gbash
if "%1" == "sh" (
	rem runas /user:Chris@Chris-Acer gbash.vbs
  gbash.vbs
) 


REM open accounts spreadsheet, go to entry
if "%1" == "ac" (
  datafromexcel.vbs %2 
) 


REM testing vbscript
if "%1" == "vbs" (
	start test.vbs
	rem test.vbs
	rem test.vbs
	rem cscript "C:\Users\Chris\scripts\test.vbs"
	rem start "" cmd /c cscript "C:\Users\Chris\scripts\test.vbs"
)

REM open folder
explorer "%1"