Option Explicit


REM where is the Excel file located?
Dim strPath, strFileName
strPath = "C:\Users\Chris\Desktop\ONSTANCE\sysadmin\"
strFileName = "Accounts.xlsm"


Dim strSearch
strSearch = 0
On Error Resume Next
strSearch = WScript.Arguments(0)
On Error Goto 0


Dim objExcel
Set objExcel = GetExcel()
objExcel.Visible = True


Dim objWorkbook
Set objWorkbook = OpenWorkbook(objExcel, strPath, strFileName)


Dim objShell
Set objShell = CreateObject("WScript.Shell")
objShell.AppActivate objExcel.Caption


objWorkbook.activate
If strSearch <> "" Then
	objExcel.Run "Accounts.xlsm!SelectClient", "" & strSearch
End If



Function GetExcel()
	Dim xlApp
	Set xlApp = Nothing
	On Error Resume Next
	Set xlApp = GetObject(,"Excel.Application")
	On Error Goto 0
	If xlApp Is Nothing Then
		'no instance of Excel running, so create one
		Set xlApp = CreateObject("Excel.Application")
		If xlApp Is Nothing Then
			MsgBox "Fatal error"
			Exit Function
		End If
	End If
	Set GetExcel = xlApp
End Function

Function OpenWorkbook(objExcel, strPath, strFileName)
	Dim isOpen
	Dim objWorkbook

	On Error Resume Next
	isOpen = Len(objExcel.Workbooks(strFileName).Name) > 0
	On Error Goto 0

	If isOpen Then
		Set objWorkbook = objExcel.Workbooks(strFileName)
	Else
		Set objWorkbook = objExcel.Workbooks.open (strPath & strFileName, false)
	End if

	Set OpenWorkbook = objWorkbook

End Function

