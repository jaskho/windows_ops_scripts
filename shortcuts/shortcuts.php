<?php
use Symfony\Component\Yaml\Yaml;

class Shortcuts{
	
	private $config;
	private $command_key;
	private $app_tag;
	private $app_key;
	private $client_tag;
	private $client_key;
	private $client_app_subkey_tag;  # ie., for multiple sublime projects under one client
	private $client_app_subkey_key;  # ie., for multiple sublime projects under one client

	private $shortcut_def;

	/**
	 * cache compiled accounts info
	 * @var [array]
	 */
	private $accounts_mgr;

	public function __construct( $args) {
		
		$this->load_config();
		$this->accounts_mgr = new ShortcutsAccounts($this->config);

		// check if this is a command (otherwise it's an "app")
		$this->command_key = $this->get_command_def($args[1]);
		if( $this->command_key ) return;

		// app tag
		if( $args[1] ) {
			$this->app_tag = $args[1];
			$this->app_key = $this->get_app_key_from_tag($args[1]);
			if(!$this->app_key) throw new Exception('Invalid application specified');
		} else {
			return;
		}


		// client tag
		if( $args[2] ) {
			$data = explode('/' , $args[2]);
			$this->client_tag = $data[0];
			$this->client_key = $this->get_client_key_from_tag($data[0]);
			if(!$this->client_key) throw new Exception('Invalid client specified');
			// client_app_subkey
			if( @$data[1] ) {
				$this->client_app_subkey_tag = $data[1];
				$this->client_app_subkey_key = $this->get_client_app_subkey_from_tag($data[1]);
				if(!$this->client_app_subkey_key) throw new Exception('Invalid client app subkey specified');
			} 		
		} else {
			return;
		}

		// shortcut
		$this->get_shortcut_def();


	}


	public function execute($args = array()){

		if( $this->command_key ) {
			$comm = $this->config['commands'][$this->command_key]['method'];
			#"list_clients";
			$this->$comm( $args );

		} else {
			$comm = '"' . $this->shortcut_def['command'] . '"' ;
			$comm = $this->shortcut_def['command'] ;
			print "command: " . $comm;
			`$comm`;
		}
	}


	private function load_config(){
		$config = file_get_contents( __DIR__ . '/data/config.yaml');
		$this->config = Yaml::parse($config);

		// $op = Yaml::dump($this->config, 100);
		// echo $op;
	}


	private function get_command_def($key){
		if( array_key_exists($key, $this->config['commands']) ) return $key;
		return null;
	}

	private function get_app_key_from_tag($tag){
		foreach( $this->config['apps'] as $app_key => $app ) {
			if( $app['tag'] == $tag ) return $app_key;
		}
		return null;
	}

	private function get_client_key_from_tag($tag){
		foreach( $this->config['clients'] as $client_key => $client ) {
			if( $client_key == $tag ) return $client_key;
		}
		return null;
	}

	/**
	 * [get_client_app_subkey_from_tag description]
	 * @param  [type] $tag [description]
	 * @return [type]      [description]
	 *
	 */
	private function get_client_app_subkey_from_tag($tag) {
		foreach( $this->config[ 'clients' ][ $this->client_key ][ 'apps' ][ $this->app_key ] as $subkey ){
			if( $subkey['tag'] == $tag ) return $subkey['key'];
		}
		return null;
	}

	private function get_client_app_shortcut_params($key) {
		foreach( $this->config[ 'clients' ][ $this->client_key ][ 'apps' ][ $this->app_key ] as $subkey ){
			if( $subkey['key'] == $key ) {
				return array_diff_key( $subkey , array( 'tag' => null, 'key' => '' ) );
			}
		}
		return null;
	}

	private function get_shortcut_def(){
		$app_config = $this->config['apps'][ $this->app_key ];
		$client_app_config = $this->config['clients'][$this->client_key][ 'apps' ][$this->app_key];
		if( ! $this->client_app_subkey_key ) {
			$params = $client_app_config[0];
		} else {
			$params = $this->get_client_app_shortcut_params($this->client_app_subkey_key);
		}
		$shortcut_def = array(
			'command_pattern' => $app_config[ 'command_pattern' ] ,
			'params' => $params ,
		);

		// apply replacements
		$patts = array_keys( $shortcut_def['params'] );
		$vals = array_values( $shortcut_def['params']);
		foreach($patts as &$patt) $patt = "/\%$patt/";
		$shortcut_def['command'] = preg_replace($patts, $vals, $shortcut_def['command_pattern']);
		$this->shortcut_def = $shortcut_def;

	}

	public function list_clients() {

		echo "\n** Client Tags **\n";
		$idx = 0;
		$keymap = array();
		foreach($this->config['clients'] as $key => $client ) {
			$idx++;
			$keymap[$idx] = $key;
			echo str_pad($idx . ".", 6) . str_pad($key,12) . "\n";
		}

		echo "Are you sure you want to do this?  Type 'yes' to continue: ";
		$handle = fopen ("php://stdin","r");
		$line = fgets($handle);
		$input = (int)trim($line);
		print_r($keymap);
		if( is_int($input) && $input > 0 && $input <= $idx) {
			$key = $keymap[ $input ];
			echo "\n\nKEY: " . $key . " --- Idx: " . $input . "\n\n";
			$data = array_intersect_key($this->config['clients'], array($key => null));
			$print_hdr = "\n**  Data for client '{$key}'  **\n";
			$this->print_data($data, $print_hdr);
		} else {
			echo "Will do NOTHING\n";

		  exit;
		}
		

	}

	// Open folder in Explorer - command handler
  private function open_explorer($args) {
  	
		$path = $args[2];
  	$exp = new COM("WScript.Shell");
  	$exp->Run("explorer $path");
  	$exp->AppActivate("Explorer");
  	sleep(1);
  	// Put cursor in address bar to facillitate drilling down
  	$exp->SendKeys("%d{Right}");
  	unset($exp);
  }

	private function get_all_accounts_nodes(){



	}

	private function print_accounts_info( $args = array() ){
		$tree_path = array();

		if( array_key_exists(2, $args) && ! empty($args[2] )) {
			$tree_path = explode('/', $args[2]);
		}
		$data = $this->accounts_mgr->get_all_accounts();

		if(empty($tree_path)) {
			$this->print_data($data, $header = "\n**  ALL ACCOUNTS INFO  **\n\n" );
		} else {

			// look for an item with this key.  Check clients first, then
			// misc accounts.
print_r($tree_path);
			$tree_root = $this->config['clients'];
		

		}


	}

	/** 
	 * echoes array as yaml
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	private function print_data($data, $header = "" ){
		echo $header;
		$op = Yaml::dump($data, 100);
		echo $op;
	}


}

class ShortcutsAccounts{
	private $config;
	private $accounts_data;
	private $accounts_search_index;

	public function __construct($config) {
		$this->config = $config;
	}

	public function get_all_accounts() {
		if( ! isset($this->accounts_data)) {

			$data = array();

			// client nodes
			foreach($this->config['clients'] as $key => $client ) {
				if(array_key_exists('accounts', $client)) {
					$search_index = array(
						$key,
						@$client['name'],
						@$client['tags'],
					);
					$search_index = implode(' ' , $search_index);
					$data['client_' . $key] = array( 
						'type' => 'client' , 
						'key' => $key, 
						'data' => $client['accounts'],
						'search_index' => $search_index,
					);
				}
			}

			// misc nodes
			foreach($this->config['misc_accounts'] as $key => $account ) {
				$search_index = array(
					$key,
					@$account['label'],
					@$account['tags'],
				);
				$search_index = implode(' ' , $search_index);
				$data['misc_' . $key] = array( 
					'type' => 'misc' , 
					'key' => $key, 
					'data' => $account,
					'search_index' => $search_index,	
				);
				
			}		

			$this->accounts_data = $data;
		}

		return $this->accounts_data;		
	}

	public function search_accounts($search_term){
		// $data = array();
		// foreach($this->accounts_data as $key => $item){
		// 	$temp_idx = array(
		// 		$key,
		// 		@$item['tags'],
		// 		@$item['tags'],

		// 	);

		// }
	}

	/**
	 * create search index for accounts info
	 * @return [type] [description]
	 */
	private function index_accounts(){
		// $accounts_search_index = array();

		// foreach($this->accounts_data as $key => $item){
			
		// 	$index_base_data = '';
		// 	if($item['type'] == 'client' ) {
		// 		$client = $this->config['clients'][ $item['key'] ];
		// 		$data_items = array(
		// 			$item['key'],
		// 			$client['name'],
		// 			$client['tags'],
		// 		);
		// 		$index_data = implode(' ' , $data_items);
		// 		$accounts_search_index[ $key ] = $index_data;
		// 		walk_accounts_tree(
		// 			&$client['accounts'], $item['key'], 
		// 			$key = '', 
		// 			$ancestor_data = '');
		// 	} else {

		// 	}
		// 	$index_base_data = 

		// }
		// $this->accounts_search_index = $accounts_search_index;
	}

	/**
	 * recursively compile search index data
	 * @param  [type] $root 					[description]
	 * @param  [type] $root_path			eg client/grp/subgrp
	 * @param  [type] $key 						[description]
	 * @param  [type] $ancestor_data 	all ancestor idx terms
	 * @return [type]       [description]
	 *
	 * like this:
	 * client {client idx}
	 *   + group 1 {group idx}
	 *     + item 1.a {client idx + group idx + item idx}
	 *     + group 1.1
	 *       + item 1.1.a {client idx + group 1 idx + group 1.1 idx + item 1.1.a idx}
	 *  etc.
	 */
	private function walk_accounts_tree(&$root, $root_path, $key = '', $ancestor_data = '') {
		// // is item...
		// if( array_key_exists('account_id', $root) ) {
		// 	$item_data = array(
		// 		$key,
		// 		$ancestor_data,
		// 		@$root['tags'],
		// 		@$root['label'],
		// 		@$root['notes']
		// 	);
		// 	$item_data = implode(' ' , $item_data);

		// // or group?
		// } else {
		// 	$item_data = array(
		// 		$key,
		// 		$ancestor_data,
		// 		@$root['tags'],
		// 	);
		// 	$item_data = implode(' ' , $item_data);		
		// 	foreach($root as $key => $item){
		// 		$this->walk_accounts_tree($item, $key, $item_data);
		// 	}	
		// }

	}
}

?>